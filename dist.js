

class clapCdn {
    constructor (constructorData) {
        // Default Values
        constructorData.name = (constructorData.name === undefined) ? 'Publisher' : constructorData.name
        constructorData.url = (constructorData.url === undefined) ? 'wss://weallclap.com/rcwc/' : constructorData.url

        this.username = constructorData.username
        this.password = constructorData.password
        this.eventToken = constructorData.eventToken
        this.organisationId = constructorData.organisationId
        this.onSessionTokenUpdate = constructorData.sessionTokenCallback
        this.onPublisherEstablished = constructorData.onPublisherEstablished
        this.onSubscriberEstablished = constructorData.onSubscriberEstablished
        this.onClientMessage = constructorData.onClientMessage
        this.onEventReady = constructorData.onEventReady
        this.onMediaError = constructorData.onMediaError
        this.renderWithCanvas = constructorData.renderWithCanvas
        this.connectPublisherLoopback = constructorData.connectPublisherLoopback
        this.name = constructorData.name
        this.url = constructorData.url
        this.browserType = constructorData.osEditor
        this.audioConfig = constructorData.audio
        this.videoConfig = constructorData.video

        this.websocketAPI = '1.0.0.0'
        this.sessionToken = constructorData.sessionToken
        this.sessionTokenBackup = null

        // Base Config
        this.publisherConfig =  {
            audio: true,
            video: {
                width: { ideal: 1280 },
                height: { ideal: 720 },
                facingMode: 'user'
            },
            iceServers: [
                {
                    urls: 'stun:stun.rivercdn.com:3478'
                }
            ]
        }
        
        // Override the config based on new values
        if (this.audioConfig !== undefined) {
            Object.assign(this.publisherConfig.audio, this.audioConfig);
        }
        if (this.videoConfig !== undefined) {
            Object.assign(this.publisherConfig.video, this.videoConfig);
        }
        

        this.videoRoomInfo = null;
        this.subscriberStreams = {}
        this.eventData = null;
        this.addEventRequest = false;
        this.streamList = [];
        this.videoElements = [];
        this.publisherStreams = {};
        this.publisherStream = null;

        this.subscriberConnected = false;
        
        this.readyToPublish = false;
        this.sentPublisherRequest = false;
        this.requestedToPublish = false;

        this.wsInit();
    }

    wsInit () {
        var $$ = this
        let url = this.url
        this.websocketFirstLoad = true;
        this.websocket = new WebSocket(url);
        this.websocket.onopen = function (ev) {
            $$.onOpen(ev)
        };
        this.websocket.onclose = function (ev) {
            $$.onClose(ev)
        };
        this.websocket.onerror = function (ev) {
            $$.onError(ev)
        };
        this.websocket.onmessage = function (ev) {
            $$.onMessage(ev)
        };
    }

    onOpen (ev) {
        console.log("Websocket - Connection Open", ev);
        this.wsStatus()
    }

    onClose (ev) {
        console.log("Websocket - Connection to server Closed. Resetting", ev);
        this.wsReset()
    }

    onError (ev) {
        // Restart Websocket Connection on Error
        console.log("Websocket - Connection to server Encountered an Error", ev);
        //clapCdn.websocketReset();
    }

    onMessage (ev) {
        // console.debug("WS On Message", ev)
        try {
            var success = true
            var messageData = JSON.parse(ev.data);
        } catch(err) {
            success = false
            console.error('Websocket - Recieved Message Contained an error converting JSON Object Raw Message:', ev.data, err);
            //navigation_load('home_page| ');
        }
        if (success){
            // Test to see if the API returned matches the expected API version. Only nessecary for future release test to ensure compadability.
            if (messageData['API'] != this.websocketAPI) {
                console.warn('Websocket - Websocket API does not Match. It should be:' + this.websocketAPI + ' But it is:' , messageData['API']);
            }
            // Wrapped the main command handler in a try statment to not crash the whole program if an error is encountered.
            try {
                
                // Actions to perform when connection is first established
                if (this.websocketFirstLoad === true){
                    // Grab the Session Token on first message
                    if (messageData.sessionToken) {
                        if (this.sessionToken === null || this.sessionToken === undefined) {
                            this.sessionToken = messageData.sessionToken
                            this.onSessionTokenUpdate(this.sessionToken)
                            // window.sessionTokenBackup = messageData['sessionToken']
                            console.log('Websocket - Session token form Server', messageData.sessionToken)
                        } else {
                            console.log('Websocket - We already have a Session Token From Server. But We will store the new one as a backup')
                            this.sessionTokenBackup = messageData['sessionToken']
                            messageData.sessionToken = this.sessionToken
                            this.wsStatus()
                        }
                        //this.wsLogin();
                        
                    } else {
                        console.log('Websocket - No session token form Server. Resetting', messageData)
                    }
                    this.websocketFirstLoad = false;
                }
                // console.log("Message from server", messageData);
                if (messageData.sessionToken === this.sessionToken){
                    
    /////////////////////////////////////////////////////////////
    // Returned Messsage Handlers Bellow
    /////////////////////////////////////////////////////////////
                    
                    // Info ////////
                    if (messageData['type'] === 'info') {
                        if (messageData.subModule === 'autho') {
                            if (messageData.status === 'Authenticated') {
                                console.log('Autho: Connection Authenticated')
                                this.addEvent(this.eventToken, this.organisationId)
                            } else {
                                console.log('Autho: Connection Error', messageData)
                                // if (this.sessionToken !== this.sessionTokenBackup) {
                                //     this.sessionToken = this.sessionTokenBackup
                                //     this.onSessionTokenUpdate(this.sessionToken)
                                //     // this.sentPublisherRequest = false
                                //     if (this.username !== undefined && this.password !== undefined) {
                                //         this.wsLogin();
                                //     } else {
                                //         this.wsStatus();
                                //     }
                                // }
                            }
                        } else if (messageData['status'] === 'Connection Successful') {
                            // this.wsLogin()
                            // this.wsStatus()
                            if (this.username !== undefined && this.password !== undefined) {
                                this.wsLogin();
                            }
                        }
                    } else if (messageData.type === 'returnData') {
                        if (messageData.subModule === 'eventManager') {
                            if (messageData.line === 'newEvent') {
                                console.log('Event Data Returned Successfully', messageData.data)
                                this.eventData = messageData.data
                                this.joinEvent(this.eventToken);
                                this.readyToPublish = true;
                                if (this.publisherStream !== null) {
                                    this.sentPublisherRequest = false
                                    this.streamList.splice(0, this.streamList.length)
                                    this.joinEventPublisher()
                                    // for (let publisherPc in this.publisherStreams) {
                                    //     this.iceRestart(this, this.publisherStreams[publisherPc])
                                    // }
                                } else if (this.requestedToPublish && Object.keys(this.publisherStreams).length === 0) {
                                    this.joinEventPublisher()
                                }
                                if (this.onEventReady !== undefined) {
                                    this.onEventReady()
                                }
                            } else if (messageData.line === 'videoSessionInfo') {
                                if (this.videoRoomInfo === null) {
                                  this.videoRoomInfo = new Object()
                                  this.videoRoomInfo.subscriber = new Object()
                                }
                                
                                // console.log('MEDIA Video Session Info', messageData.data)
                                if (messageData.videoType === 'publisher') {
                                    this.videoRoomInfo[messageData.videoType] = messageData.data
                                    if (this.publisherStreams[messageData.data.videoSessionId] === undefined && Object.keys(this.publisherStreams).length === 0) {
                                       this.createRTCPublisher(messageData.data.videoSessionId)
                                    } else {
                                        console.warn('skipping Creation of publisher because there is already a pc', this.publisherStreams[messageData.data.videoSessionId])
                                    }
                                } else {
                                    this.videoRoomInfo[messageData.videoType][messageData.videoSessionId] = messageData.data
                                }
                                this.eventMediaUpdate();
                            } else if (messageData.line === 'videoSessionJSEPreturnOffer') {
                                console.debug('Returned Remote Description', messageData.data.jsep)
                                this.publisherStreams[messageData.videoSessionId].setRemoteDescription(new RTCSessionDescription(messageData.data.jsep))
                                this.startBroadcast(messageData.videoSessionId)
                            } else if (messageData.line === 'videoBroadcastMediaUpdate' && this.videoRoomInfo !== null) {
                                // console.log('this.videoRoomInfo', this.videoRoomInfo)
                                if (this.videoRoomInfo.subscriber[messageData.videoSessionId] === undefined) {
                                    this.videoRoomInfo.subscriber[messageData.videoSessionId] = {}
                                    // console.log('New Video Session From Server', messageData.videoSessionId)
                                }
                                if (this.videoRoomInfo.subscriber[messageData.videoSessionId].publisherIds === undefined) {
                                    this.videoRoomInfo.subscriber[messageData.videoSessionId].publisherIds = {}
                                    // console.log('Defined Publisher IDs')
                                }
                                if (this.videoRoomInfo.subscriber[messageData.videoSessionId].publisherIds[messageData.data.broadcastId] === undefined) {
                                    this.videoRoomInfo.subscriber[messageData.videoSessionId].publisherIds[messageData.data.broadcastId] = {}
                                    this.videoRoomInfo.subscriber[messageData.videoSessionId].publisherIds[messageData.data.broadcastId].type = {}
                                    this.videoRoomInfo.subscriber[messageData.videoSessionId].publisherIds[messageData.data.broadcastId].broadcastId = messageData['data']['broadcastId']
                                    // console.log('Defined Publisher IDs')
                                }
                                for (var mediaType in messageData['data']['type']) {
                                    this.videoRoomInfo.subscriber[messageData.videoSessionId].publisherIds[messageData.data.broadcastId].type[mediaType] = messageData['data']['type'][mediaType]
                                    console.log('MEDIA UPDATE', messageData.data.broadcastId, mediaType, messageData['data']['type'][mediaType])
                                }
                                this.eventMediaUpdate();
                            } else if (messageData.line === 'videoSupscriptionJSEPreturnOffer') {
                                if (this.videoRoomInfo.subscriber[messageData.videoSessionId] === undefined) {
                                    this.videoRoomInfo.subscriber[messageData.videoSessionId] = {}
                                    // console.log('New Video Session From Server in Subscription', messageData.videoSessionId)
                                }
                                if (this.videoRoomInfo.subscriber[messageData.videoSessionId].subscriptionFeeds === undefined) {
                                    this.videoRoomInfo.subscriber[messageData.videoSessionId].subscriptionFeeds = {}
                                    // console.log('Defined Publisher IDs')
                                }
                                this.videoRoomInfo.subscriber[messageData.videoSessionId].subscriptionFeeds[messageData.data.feedId] = messageData.data
                                this.updateSubFeeds(messageData.videoSessionId)
                            }
                        }
                    } else if (messageData.type === 'clientRelay') {
                        if (this.onClientMessage !== undefined) {
                            this.onClientMessage(messageData.data) 
                        }
                    }
                } else {
                    console.warn("Websocket - SessionToken Doesn't match. Connection may have been Reset. Using Backup and Re Authenticating", messageData)
                    this.sessionToken = messageData.sessionToken
                    this.onSessionTokenUpdate(this.sessionToken)
                    // this.sentPublisherRequest = false
                    if (this.username !== undefined && this.password !== undefined) {
                        this.wsLogin();
                    } else {
                        this.wsStatus();
                    }
                }
            } catch(err) {
                console.error('Websocket - Message Processing(After JSON Converted) Contained an Error', err, messageData);
                //navigation_load('home_page| ');
            }
        }
    }

    // Websocket - Send Function - Recieves the constructed object to send
    send (messageData) {
        messageData['API'] = this.websocketAPI; // Add Websocket API Number to our command
        
        // Use Example of Websocket Autho wait to wait for Authentication Login before Sening
        if (this.sessionToken === null){
            console.log('Websocket - No session Token when trying message to send to server')
        } else {
            messageData['sessionToken'] = this.sessionToken;
            try {
                var json_data = '';
                
                try {
                    var success = true;
                    json_data = JSON.stringify(messageData);
                } catch(err) {
                    console.error('Websocket - converting Message to JSON contained an error.', err, messageData);
                    success = false;
                    return false;
                }
                
                if (success) {
                    this.websocket.send(json_data);
                    return true;
                    // console.log('Websocket - Sent Message Successful', messageData, json_data);
                }
                
            } catch(err) {
                console.error('Websocket - Sending message failed', err, messageData);
                return false;
            }
        }
    }

    // Starts the Websocket Connection Loop
    wsReset () {
        var $$ = this;
        for (let publisherPc in this.publisherStreams) {
            this.publisherStreams[publisherPc].close()
            delete this.publisherStreams[publisherPc]
        }
        for (let subscriberPc in this.subscriberStreams) {
            this.removeTracks(this.subscriberStreams[subscriberPc])
            this.subscriberStreams[subscriberPc].close()
            delete this.subscriberStreams[subscriberPc]
        }
        setTimeout(function() {
            if ($$.websocketFirstLoad) {
                console.log('Websocket - Attempting to Connect');
            } else {
                console.log('Websocket - Attempting to Reconnect');
            }
            $$.websocketFirstLoad = true;
            $$.wsInit();
        }, 2000);
    }

    wsLogin () {
        this.send({
            type: 'autho',
            line: 'login',
            username: this.username,
            password: this.password
        })
    }

    wsStatus () {
        this.send({
            type: 'autho',
            line: 'getStatus',
        })
    }

    detectBrowser () {
        // Opera 8.0+
        // var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;

        // Firefox 1.0+
        // var isFirefox = typeof InstallTrigger !== 'undefined';

        // Safari 3.0+ "[object HTMLElementConstructor]" 
        /* eslint-disable-next-line no-undef */
        var isSafari = /constructor/i.test(window.HTMLElement) || (function (p) { return p.toString() === "[object SafariRemoteNotification]"; })(!window['safari'] || (typeof safari !== 'undefined' && safari.pushNotification));

        // Internet Explorer 6-11
        // var isIE = /*@cc_on!@*/false || !!document.documentMode;

        // Edge 20+
        // var isEdge = !isIE && !!window.StyleMedia;

        // Chrome 1 - 79
        var isChrome = !!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime);

        // Edge (based on chromium) detection
        // var isEdgeChromium = isChrome && (navigator.userAgent.indexOf("Edg") != -1);

        // Blink engine detection
        // var isBlink = (isChrome || isOpera) && !!window.CSS;

        if (isSafari) {
            return 'safari'
        } else if (isChrome) {
            return 'chrome'
        }

    }

    addEvent (token, organisationId) {
        if (!this.addEventRequest) {
            this.send({
                type: 'eventManager',
                line: 'addEvent',
                data: {
                    token: token,
                    organisationId: organisationId
                }
            })
            this.addEventRequest = true
        }
    }

    joinEvent (token) {
        this.send({
            type: 'event',
            line: 'changeEvent',
            eventToken: token
        })
    }

    sendClientMessage (data) {
        return this.send({
            type: 'clientRelay',
            line: 'clientRelay',
            data: data
        })
    }

    requestToPublish () {
        this.requestedToPublish = true;
        this.joinEventPublisher()
    }

    joinEventPublisher () {
        if (!this.sentPublisherRequest && this.readyToPublish) {
            var broadcastName = this.name
            // if (this.browserType === '' || this.browserType === undefined) {
            //     if (this.detectBrowser() === 'safari') {
            //         broadcastName += '[safari]'
            //     } else if (this.detectBrowser() === 'chrome') {
            //         broadcastName += '[chrome]'
            //     }
            // } else {
            //     broadcastName += '[' + this.browserType + ']'
            // }
            this.send({
                type: 'eventManager',
                line: 'joinEventVideo',
                eventId: this.eventToken,
                roll: 'publisher',
                data: {
                    display: broadcastName
                }
            })
            this.sentPublisherRequest = true
        } else {
            console.warn('Publisher Request already sent or connection is not ready. The CDN will fire the publisher command when it is ready')
        }
    }

    joinEventSubscriber (token) {
        this.send({
            type: 'eventManager',
            line: 'joinEventVideo',
            eventId: token,
            roll: 'subscriber',
            data: {
                display: 'Subscriber'
            }
        })
    }

    startBroadcast (videoSessionId) {
        if (this.onPublisherEstablished !== undefined) {
            this.onPublisherEstablished()
        }
        this.send({
            type: 'eventManager',
            line: 'startBroadcast',
            videoSessionId: videoSessionId,
            eventId: this.eventToken
        })
    }

    startRecording (deleteMedia) {
        if (deleteMedia === undefined || deleteMedia === null) {
            deleteMedia = false
        }
        let message = {
            type: 'eventManager',
            line: 'startEventRecording',
            eventId: this.eventToken,
            deleteMedia: deleteMedia
        }
        console.debug('Started Event Recording', message)
        return this.send(message)
    }

    stopRecording (generateAssets) {
        if (generateAssets === undefined || generateAssets === null) {
            generateAssets = false
        }
        return this.send({
            type: 'eventManager',
            line: 'stopEventRecording',
            eventId: this.eventToken,
            generateAssets: generateAssets
        })
    }

    requestAssetProcessing (type, data) {
        return this.send({
            type: 'eventManager',
            line: 'requestAssetProcessing',
            data: data,
            mediaType: type,
            eventId: this.eventToken
        })
    }

    deleteMedia () {
        return this.send({
            type: 'eventManager',
            line: 'deleteMediaAssets',
            eventId: this.eventToken
        })
    }

    eventMediaUpdate () {
        var videoSession
        for (videoSession in this.videoRoomInfo.subscriber) {
            for (var publisherId in this.videoRoomInfo.subscriber[videoSession].publisherIds) {
                if ((this.videoRoomInfo.subscriber[videoSession].publisherIds[publisherId].type['video'] || this.videoRoomInfo.subscriber[videoSession].publisherIds[publisherId].type['audio']) && this.subscriberStreams[publisherId] === undefined) {
                    if (this.videoRoomInfo.publisher !== undefined){
                        if (this.videoRoomInfo.publisher.id.toString() !== publisherId || this.connectPublisherLoopback === true) {
                            this.createRTCSubscription(videoSession, publisherId)
                            if (!this.subscriberConnected && this.onSubscriberEstablished !== undefined) {
                                this.onSubscriberEstablished()
                                this.subscriberConnected = true;
                            }
                        }
                        // else {
                        //     console.log('Skipping Creating Media because subscription is publisher.', publisherId)
                        // }
                    } else {
                        this.createRTCSubscription(videoSession, publisherId)
                    }
                } else if (!this.videoRoomInfo.subscriber[videoSession].publisherIds[publisherId].type['video'] && !this.videoRoomInfo.subscriber[videoSession].publisherIds[publisherId].type['audio']) {
                    let subStream = null
                    // Identify pc to remove
                    for (let subscriberStream in this.subscriberStreams) {
                        if (this.subscriberStreams[subscriberStream].feedId !== undefined) {
                        // subStream = this.subscriberStreams[feedId]
                            if (this.subscriberStreams[subscriberStream].feedId.broadcastId.toString() === publisherId.toString()) {
                                subStream = this.subscriberStreams[subscriberStream]
                            }
                        }
                    }
                    if (subStream !== null) {
                        this.removeTracks(subStream)
                    }
                } else if (this.videoRoomInfo.subscriber[videoSession].publisherIds[publisherId].type['video'] || this.videoRoomInfo.subscriber[videoSession].publisherIds[publisherId].type['audio']) {
                    let subStream = null
                    // Identify pc to remove
                    for (let subscriberStream in this.subscriberStreams) {
                        if (this.subscriberStreams[subscriberStream].feedId !== undefined) {
                        // subStream = this.subscriberStreams[feedId]
                            if (this.subscriberStreams[subscriberStream].feedId.broadcastId.toString() === publisherId.toString() && this.subscriberStreams[subscriberStream].localDescription !== null) {
                                subStream = this.subscriberStreams[subscriberStream]
                            }
                        }
                    }
                    if (subStream !== null) {
                        let receivers = subStream.getReceivers()
                        let streams = new MediaStream()
                        for (let receiver in receivers) {
                            streams.addTrack(receivers[receiver].track)
                        }
                        this.onTrack(subStream, {streams: streams})
                    }
                }
            }
        }
        // if (this.videoRoomInfo.subscriber.publisherIds !== undefined) {
        //     for (var publisherId in this.videoRoomInfo.subscriber.publisherIds) {
        //         if (this.videoRoomInfo.subscriber.publisherIds[publisherId].type['video'] && this.videoRoomInfo.subscriber.publisherIds[publisherId].type['audio'] && this.subscriberStreams[publisherId] === undefined) {
        //             if (this.videoRoomInfo.publisher !== undefined){
        //                 if (this.videoRoomInfo.publisher.id.toString() !== publisherId) {
        //                     this.createRTCSubscription(videoSession, publisherId)
        //                 } else {
        //                     console.log('Skipping Creating Media because subscription is publisher.', publisherId)
        //                 }
        //             } else {
        //                 this.createRTCSubscription(videoSession, publisherId)
        //             }
        //         }
        //     }
        // }
    }

    createCanvasRender (pc, stream, loop) {
        pc.canvasRunning = true
        pc.canvasDrawLoop = loop

        // Get Stream width and Height
        let width = stream.getVideoTracks()[0].getSettings().width
        let height = stream.getVideoTracks()[0].getSettings().height
        console.debug('stream width', width, 'stream height', height)

        // Create the Canvas
        pc.canvas = document.createElement('canvas')
        pc.canvas.setAttribute('width', 1280)
        pc.canvas.setAttribute('height', 720)
        let frameRate = 30

        // Create the Hidden Video to Draw to the canvas
        pc.origionalStreamVideoElement = document.createElement('video')
        pc.origionalStreamVideoElement.muted = true
        pc.origionalStreamVideoElement.srcObject = stream
        pc.origionalStreamVideoElement.play()

        // Start the Render Loop
        let context = pc.canvas.getContext('2d')
        pc.canvasDrawLoop(pc, pc.origionalStreamVideoElement, pc.canvas, context, frameRate)

        // Create the New Video Stream from the canvas
        pc.canvasStream = pc.canvas.captureStream(30)
        pc.canvasStream.getVideoTracks()[0].applyConstraints({width: 1280, height: 720}).then(result => {
            console.log('stream constraints', result)
        })

        //Add the original audio stream to the new canvas stream
        pc.canvasStream.addTrack(stream.getAudioTracks()[0])

        return pc.canvasStream
    }

    canvasDrawLoop (pc, video, canvas, context, frameRate) {
        if (pc.canvasRunning) {
            context.drawImage(video, 0, 0, canvas.width, canvas.height)
            canvas.frameCaptureRequested = true
            setTimeout(pc.canvasDrawLoop, 1 / frameRate, pc, video, canvas, context, frameRate)
        }
    }

    getVideoSnapshot(videoEl) {
        //let source = videoEl.srcObject.getVideoTracks()[0]
        // console.debug('VIDEO SETTINGS', source.getSettings())
        // console.debug('VIDEO ELEMENT', videoEl)
        // let width = source.getSettings().width
        // let height = source.getSettings().height
        let width = videoEl.videoWidth
        let height = videoEl.videoHeight
        let canvas = document.createElement('canvas')

        canvas.setAttribute('width', width)
        canvas.setAttribute('height', height)

        let context = canvas.getContext('2d')
        context.drawImage(videoEl, 0, 0, canvas.width, canvas.height)
        var imageData = canvas.toDataURL()
        return imageData
    }

    createRTCPublisher (videoSessionId) {
        var $$ = this
        this.publisherStreams[videoSessionId] = new RTCPeerConnection(this.publisherConfig)
        this.publisherStreams[videoSessionId].videoSessionId = videoSessionId
        this.publisherStreams[videoSessionId].roll = 'publisher'
        this.publisherStreams[videoSessionId].onicecandidate = function (event) {
            $$.onIceCandidate($$, $$.publisherStreams[videoSessionId], event)
        }
        this.publisherStreams[videoSessionId].oniceconnectionstatechange = function (event) {
            $$.onIceConnectionStateChangePublisher($$, $$.publisherStreams[videoSessionId], event)
        }
        this.publisherStreams[videoSessionId].ontrack = this.onTrack
        this.publisherStreams[videoSessionId].videoSession = this.videoRoomInfo.publisher[videoSessionId]
        let onTrackPc = this.publisherStreams[videoSessionId]
        onTrackPc.displayName = $$.name
        console.warn('Defining GetUserMedia')
        var connectRTCPub = function (stream) {
            $$.publisherStream = stream
            if ($$.renderWithCanvas !== undefined && $$.renderWithCanvas === true) {
                // Render with a canvas
                let canvasStream = $$.createCanvasRender(onTrackPc, stream, $$.canvasDrawLoop)
                $$.onTrack(onTrackPc, {streams: canvasStream})
                $$.publisherStreams[videoSessionId].addStream(canvasStream)
            } else {
                // console.debug('Our Stream', stream, stream.getVideoTracks())
                // Render without a canvas
                $$.onTrack(onTrackPc, {streams: stream})
                $$.publisherStreams[videoSessionId].addStream(stream)
            }
            $$.joinEventSubscriber($$.eventToken);
            $$.publisherStreams[videoSessionId].createOffer().then(offer => {
                console.debug('SDP Offer', offer)
                $$.publisherStreams[videoSessionId].setLocalDescription(offer)
                $$.send({
                    type: 'eventManager',
                    line: 'publishEventVideo',
                    eventId: $$.eventToken,
                    jsep: offer,
                    videoSessionId: videoSessionId
                })
            })
        }
        var onRTCError = function (err) {
            console.error('getUserMedia raised an Error', err)
            if ($$.onMediaError !== undefined) {
                $$.onMediaError()
            } else {
                alert('Error accessing Camera and Microphone.\n\nYou may have denied permissions or this current browser might not support the camera and/or microphone.\n\nOn iOS you must use Safari and check the camera and microphone permission.\n\n On other platforms you can use Google Chrome or Firefox. Confirm that your connection is secure (https).')
            }
        }
        if ($$.publisherStream === null) {
            try {
                navigator.mediaDevices.getUserMedia(this.publisherConfig).then(connectRTCPub).catch(onRTCError)
            } catch(err) {
                onRTCError(err)
            }
        } else {
            connectRTCPub($$.publisherStream)
        }
    }

    createRTCSubscription (videoSessionId, publisherId) {
        if ((this.videoRoomInfo.subscriber[videoSessionId].publisherIds[publisherId]['type']['video'] || this.videoRoomInfo.subscriber[videoSessionId].publisherIds[publisherId]['type']['audio'])) {
            let createPc = true
            if (this.subscriberStreams[publisherId] !== undefined) {
                // if (this.subscriberStreams[publisherId].connectionState === 'failed' || this.subscriberStreams[publisherId].connectionState === 'disconnected') {
                if (this.subscriberStreams[publisherId].connectionState === 'failed') {
                    createPc = true
                    console.log('Creating pc because it is', this.subscriberStreams[publisherId], this.subscriberStreams[publisherId].connectionState)
                } else {
                    createPc = false
                    console.log('Not recreating pc because it is', this.subscriberStreams[publisherId], this.subscriberStreams[publisherId].connectionState)
                }
            }
            if (createPc) {
                let $$ = this
                console.warn('Creating new Subscription connection', publisherId)
                this.subscriberStreams[publisherId] = new RTCPeerConnection(this.publisherConfig)
                this.subscriberStreams[publisherId].roll = 'subscriber'
                this.subscriberStreams[publisherId].videoSessionId = videoSessionId
                this.subscriberStreams[publisherId].onconnectionstatechange = function (event) {
                    $$.onConnectionStateChange($$, event)
                }
                this.subscriberStreams[publisherId].oniceconnectionstatechange = function (event) {
                    $$.onIceConnectionStateChangeSubscriber($$, $$.subscriberStreams[publisherId], event)
                }
                this.subscriberStreams[publisherId].videoSession = this.videoRoomInfo.subscriber[videoSessionId]
                if (this.subscriberStreams[publisherId].videoSessionId === undefined) {
                this.subscriberStreams[publisherId].videoSessionId = this.videoRoomInfo.subscriber[videoSessionId].publisherIds[publisherId]
                }
                // this.subscriberStreams[publisherId].ontrack = this.onTrack
                let onTrackPc = this.subscriberStreams[publisherId]
                
                this.subscriberStreams[publisherId].ontrack = function (event) {
                    // console.log('On Track Event Fired', onTrackPc)
                    if ($$.renderWithCanvas !== undefined && $$.renderWithCanvas === true) {
                        // Render with a canvas
                        let canvasStream = $$.createCanvasRender(onTrackPc, event.streams[0], $$.canvasDrawLoop)
                        $$.onTrack(onTrackPc, {streams: canvasStream})
                    } else {
                        // console.debug('Our Stream', stream, stream.getVideoTracks())
                        // Render without a canvas
                        $$.onTrack(onTrackPc, event)
                    }
                }
                this.subscriberStreams[publisherId].feedId = this.videoRoomInfo.subscriber[videoSessionId].publisherIds[publisherId]
                var eventVideoData = {}
                eventVideoData.feedId = parseInt(publisherId)
                this.send({
                type: 'eventManager',
                line: 'joinEventVideo',
                roll: 'subscriber',
                data: eventVideoData,
                videoSessionId: null
                })
            }
        }
    }
    updateSubFeeds (videoSessionId) {
        var $$ = this;
        if (this.videoRoomInfo.subscriber[videoSessionId].subscriptionFeeds !== undefined) {
            for (let feedId in this.videoRoomInfo.subscriber[videoSessionId].subscriptionFeeds) {
              var subStream = null
              // Identify the subscriptionStream
              for (let subscriberStream in this.subscriberStreams) {
                if (this.subscriberStreams[subscriberStream].feedId !== undefined) {
                  // subStream = this.subscriberStreams[feedId]
                  if (this.subscriberStreams[subscriberStream].feedId.broadcastId.toString() === feedId.toString()) {
                    subStream = this.subscriberStreams[subscriberStream]
                  }
                }
                if (subStream !== null) {
                //   console.log('Substream', subStream, feedId)
                  if (subStream.remoteSet === undefined && subStream.remoteDescription === null && this.videoRoomInfo.subscriber[videoSessionId].subscriptionFeeds[feedId].jsep !== undefined) {
                    if (subStream.videoSessionId !== videoSessionId) {
                      if (this.videoRoomInfo.subscriber[videoSessionId].publisherIds === undefined) {
                        this.videoRoomInfo.subscriber[videoSessionId].publisherIds = {}
                      }
                      this.videoRoomInfo.subscriber[videoSessionId].publisherIds[feedId] = this.videoRoomInfo.subscriber[subStream.videoSessionId].publisherIds[feedId]
                      subStream.videoSessionId = videoSessionId
                      subStream.videoSession = this.videoRoomInfo.subscriber[videoSessionId]
                    }
                    let pc = subStream
                    subStream.onicecandidate = function (event) {
                        $$.onIceCandidate($$, subStream, event)
                    }
                    subStream.setRemoteDescription(new RTCSessionDescription(this.videoRoomInfo.subscriber[videoSessionId].subscriptionFeeds[feedId].jsep))
                    subStream.remoteSet = true
                    subStream.displayName = this.videoRoomInfo.subscriber[videoSessionId].subscriptionFeeds[feedId].plugindata.data.display
                    pc.createAnswer().then(answer => {
                      pc.setLocalDescription(answer)
                      // msg = JSON.stringify({janus: 'message', transaction: 'blah', body: {request: 'start'}, jsep: answer, session_id: sessionId, handle_id: handleId});
                      // janusConnection.send(msg);
                      $$.send({
                        type: 'eventManager',
                        line: 'startSubscriberVideo',
                        eventId: $$.eventToken,
                        jsep: answer,
                        videoSessionId: pc.videoSessionId
                      })
                    })
                  }
                }
              }
            }
          }
    }

    onIceCandidate ($$, pc, event) {
        // console.log('event on ice', event)
        $$.send({
            type: 'eventManager',
            line: 'trickleEventVideo',
            candidate: event.candidate,
            videoSessionId: pc.videoSessionId
        })
        $$.update()
    }

    onTrack (pc, event) {
        // console.debug('onTrack', event)
        // for (var stream in event.streams) {
        var duplicate = false
        for (var i = 0; i < this.streamList.length; i++) {
          if (this.streamList[i].pc === pc) {
            duplicate = i
          }
          // if (this.streamList[i].pc.connectionState === 'failed' || this.streamList[i].pc.connectionState === 'disconnected') {
        //   if (this.streamList[i].pc.connectionState === 'failed') {
        //     this.removeTracks(this.streamList[i].pc)
        //     console.log('Deleted Subscription Stream in Stream List', this.streamList[i].pc, this.streamList)
        //   }  
        }
        if (duplicate === false) {
          this.streamList.push({pc: pc, streams: event.streams})
        } else {
          this.streamList[duplicate] = {pc: pc, streams: event.streams}
        }
        // console.debug('END onTrack', pc, event)
        this.update()
        // }
        // this.addStream(pc, event.streams[0])
    }

    onConnectionStateChange ($$, event) {
        //if ((event.target.connectionState === 'failed' || event.target.connectionState === 'disconnected') && event.target.feedId !== undefined) {
        if ((event.target.connectionState === 'failed') && event.target.feedId !== undefined) {
            console.log('BROADCAST ID', event.target.feedId.broadcastId, $$.subscriberStreams)
            $$.removeTracks($$.subscriberStreams[event.target.feedId.broadcastId])
        }
        $$.update()
    }

    removeTracks (pc) {
        for (var i = 0; i < this.streamList.length; i++) {
            if (this.streamList[i].pc.videoSessionId === pc.videoSessionId) {
                this.streamList.splice(i, 1)
                if (pc.feedId !== undefined) {
                  this.createRTCSubscription(pc.videoSessionId, pc.feedId.broadcastId)
                }
            }
        }
    }

    unMuteParticipants (toMute) {
        for (var stream in this.streamList) {
            if (this.streamList[stream].pc.feedId !== undefined) {
                if (this.streamList[stream].pc.feedId.broadcastId.toString() !== this.videoRoomInfo.publisher.id.toString()) {
                    this.streamList[stream].audioElem.muted = !toMute
                }
            }
        }
    }

    setPublisherMic (enabled) {
        this.publisherStream.getAudioTracks()[0].enabled = enabled
    }

    onIceConnectionStateChangeSubscriber ($$, pc, event) {
        console.debug('RTC ON ICE Subscriber: Connection state change event', pc.iceConnectionState, 'FEED', pc.feedId, event)
        // if ((pc.iceConnectionState === 'failed') && pc.feedId !== undefined) {
        //   $$.subscriberIceRestart($$, pc)
        // }
    }

    subscriberIceRestart ($$, pc) {
        pc.restartRequested = true
        // this.socketSend({
        //   type: 'eventManager',
        //   line: 'restartIce',
        //   eventId: this.eventId,
        //   videoSessionId: pc.videoSessionId,
        //   feedId: pc.feedId
        // })
        var eventVideoData = {}
        eventVideoData.feedId = pc.feedId.broadcastId
        $$.send({
            type: 'eventManager',
            line: 'joinEventVideo',
            roll: 'subscriber',
            data: eventVideoData,
            videoSessionId: pc.videoSessionId
        })
    }

    onIceConnectionStateChangePublisher ($$, pc, event) {
        console.log('RTC ON ICE Publisher: Connection state change event', pc.iceConnectionState, 'FEED', pc.feedId, event)
        if ((pc.iceConnectionState === 'failed') && pc.feedId !== undefined) {
            this.iceRestart($$, pc)
        }
    }

    iceRestart ($$, pc) {
    // Broadcaster Only
    console.log('ICE restart Triggered', pc)
    pc.createOffer({ iceRestart: true })
        .then( function (localDescription) {
            pc.setLocalDescription(localDescription)
        })
        .then(function (offer) {
            $$.send({
                type: 'eventManager',
                line: 'publishEventVideo',
                eventId: $$.eventId,
                jsep: offer,
                videoSessionId: pc.videoSessionId
            })
        })
    }

    update () {
        // let no = $.streamList.length
        // $.streamList.map((item, index) => {
        //     index
        //     let id = item.pc.videoSessionId
        //     let dom = document.querySelector('#ag-item-' + id)
            // if (!dom) {
            //   dom = document.createElement('section')
            //   dom.setAttribute('id', 'ag-item-' + id)
            //   dom.setAttribute('class', 'ag-item')
            //   canvas.appendChild(dom)
            //   item.play('ag-item-' + id)
            // }
            //// dom.setAttribute('style', `grid-area: ${tileCanvas[no][index]}`)
            // item.player.resize && item.player.resize()
            // })
        // let $ = this
        // // rerendering
        // let canvas = document.querySelector('#ag-canvas')
        // // pip mode (can only use when less than 4 people in channel)
        // if ($.displayMode === 'pip') {
        //   let no = $.streamList.length
        //   if (no > 4) {
        //     $.displayMode = 'tile'
        //     return
        //   }
        //   $.streamList.map((item, index) => {
        //     let id = item.pc.sessionId
        //     let dom = document.querySelector('#ag-item-' + id)
        //     if (!dom) {
        //       dom = document.createElement('section')
        //       dom.setAttribute('id', 'ag-item-' + id)
        //       dom.setAttribute('class', 'ag-item')
        //       canvas.appendChild(dom)
        //       item.play('ag-item-' + id)
        //     }
        //     if (index === no - 1) {
        //       dom.setAttribute('style', `grid-area: span 12/span 24/13/25`)
        //     } else {
        //       dom.setAttribute(
        //         'style',
        //         `grid-area: span 3/span 4/${4 + 3 * index}/25
        //       z-index:1width:calc(100% - 20px)height:calc(100% - 20px)`
        //       )
        //     }
        //     item.player.resize && item.player.resize()
        //   })
        // } else if ($.displayMode === 'tile') {
        //   // tile mode
        //   let no = $.streamList.length
        //   $.streamList.map((item, index) => {
        //     let id = item.pc.videoSessionId
        //     let dom = document.querySelector('#ag-item-' + id)
        //     if (!dom) {
        //       dom = document.createElement('section')
        //       dom.setAttribute('id', 'ag-item-' + id)
        //       dom.setAttribute('class', 'ag-item')
        //       canvas.appendChild(dom)
        //       item.play('ag-item-' + id)
        //     }
        //     dom.setAttribute('style', `grid-area: ${tileCanvas[no][index]}`)
        //     item.player.resize && item.player.resize()
        //   })
        // } else if ($.displayMode === 'share') {
        //   // screen share mode (tbd)
        // }
        for (var pubId in this.subscriberStreams) {
            if (this.subscriberStreams[pubId].connectionState === 'failed') {
                this.removeTracks(this.subscriberStreams[pubId])
                console.log('Deleted Subscription Stream', pubId, this.subscriberStreams)
            }
        }
        // setTimeout(function() {
        //     for (var pubId in $.subscriberStreams) {
        //         console.log('Connection State on PC test in callback is', $.subscriberStreams[pubId], $.subscriberStreams[pubId].connectionState)
        //         if ($.subscriberStreams[pubId].connectionState === 'failed' || $.subscriberStreams[pubId].connectionState === 'disconnected') {
        //             $.removeTracks($.subscriberStreams[pubId])
        //             console.log('Deleted Subscription Stream', pubId, $.subscriberStreams)
        //         }
        //     }
        // }, 2000);
        for (var stream in this.streamList) {
            var videoTrack = this.streamList[stream].streams
            var audioTrack = null
            if (this.streamList[stream].streams[0] !== undefined && this.streamList[stream].streams[0].getAudioTracks !== undefined) {
                var audioTracks = this.streamList[stream].streams[0].getAudioTracks()
                audioTrack = new MediaStream()
                audioTrack.addTrack(audioTracks[0])
            }
            
            // var audioElement = null
            // for (var track in this.streamList[stream].streams) {
            //   if (this.streamList[stream].streams[track].kind === 'audio') {
            //     audioElement = this.streamList[stream].streams[track]
            //   } else if (this.streamList[stream].streams[track].kind === 'video') {
            //     videoElement = this.streamList[stream].streams[track]
            //   }
            // }
            if (Array.isArray(videoTrack)) {
                videoTrack = videoTrack[0]
            }
            // console.log('AV', videoElement, audioElement)
            // console.log('AV', this.streamList[stream])
            if (videoTrack !== null) {
                var videoDomElement = document.getElementById('video' + this.streamList[stream].pc.videoSessionId)
                // console.log('DOM Object', videoDomElement)
                if (videoDomElement !== null) {
                    if (videoDomElement.hasSource === undefined) {
                        videoDomElement.srcObject = videoTrack
                        videoDomElement.hasSource = true
                        this.streamList[stream].videoElem = videoDomElement;
                    }
                }
            }
            if (audioTrack !== null) {
                var audioDomElement = document.getElementById('audio' + this.streamList[stream].pc.videoSessionId)
                if (audioDomElement !== null) {
                    // console.log('video scr element', videoDomElement.srcObject)
                    if (audioDomElement.hasSource === undefined) {
                        audioDomElement.srcObject = audioTrack
                        audioDomElement.hasSource = true
                        this.streamList[stream].audioElem = audioDomElement;
                    }
                }
        }
        // if (audioElement !== null) {
        //   var audioDomElement = document.getElementById('audio' + this.streamList[stream].pc.videoSessionId)
        //   console.log('DOM Object', audioDomElement)
        //   if (audioDomElement !== null) {
        //     audioDomElement.srcObject = audioElement
        //   }
        // }
        }
    }

}